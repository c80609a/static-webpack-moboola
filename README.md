# README

This repository compiles static assets for prelands using your local workstation.

### Requirements:
 - nodejs 5+
 - yarn

### Install
```bash
npm i
```

### To compile
```bash
yarn build
```

### To compile for DEV
```bash
yarn build_dev
```
  
### Result
```bash
dist/static.js
dist/static.сss
```

### To upload

```bash
cd /home/scout/git/socapp
rails c
```

Then:
```ruby
load '/home/scout/git/socapp/lib/utils/aws/upload_static_preland_js.rb'
```

It uploads `static.js`+`static.css` to AWS bucket.

