const path = require('path')
const webpack = require('webpack')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
const CompressionPlugin = require('compression-webpack-plugin')
const ImageminPlugin = require('imagemin-webpack-plugin').default
const imageminMozjpeg = require('imagemin-mozjpeg')
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
  entry: './public/index.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'static.js'
  },
  module: {
    rules: [
      {
        test: /\.(scss|sass|css)$/i,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            { loader: 'css-loader', options: { minimize: true } },
            { loader: 'postcss-loader', options: { plugins: [require('autoprefixer')] } },
            'sass-loader'
          ]
        })
      },
      {
        test: /\.coffee$/,
        loaders: ['coffee-loader']
      },
      {
        test: /\.(jpg|jpeg|png|gif|svg|eot|ttf|woff|woff2|otf)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name]-[hash].[ext]'
            }
          }
        ]
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: 'html-loader',
            options: {
              root: '.',
              attrs: ['img:src'],
              options: {
                minimize: true
              }
            }
          }
        ]
      }
    ]
  },
  optimization: {
    minimizer: process.env.NODE_ENV == 'development' ? [] : [new UglifyJsPlugin()]
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV':    JSON.stringify(process.env.NODE_ENV),
        'API_URL':     JSON.stringify(process.env.API_URL || 'https://datemehere24.eu'),
        'CONTINUE_ID': JSON.stringify(process.env.CONTINUE_ID || 'continue'),
        'API_UID':     JSON.stringify('cb3418215dc28e8ff60f5259b5ff071e899e31587d6fef87bb24734f05df41ac'),
        'API_SECRET':  JSON.stringify('62e31a51f19299f748c9a85c13ee160df04b62a0649d698332edf01b34d6738c')
      }
    }),
    new CleanWebpackPlugin('dist/*'),
    new ExtractTextPlugin('static.css'),
    new OptimizeCssAssetsPlugin(),
    new ImageminPlugin({
      test: /\.(jpg|jpeg|png|gif|svg)$/,
      maxFileSize: 10000
    }),
    new ImageminPlugin({
      test: /\.(jpg|jpeg|png|gif|svg)$/,
      minFileSize: 10000,
      jpegtran: null,
      pngquant: { quality: '80-90' },
      plugins: [
        imageminMozjpeg({ quality: 80, progressive: true })
      ]
    }),
    // new HtmlWebpackPlugin({
    //   template: './public/src/index.html'
    // }),
    new CompressionPlugin({
      filename: '[path].gz[query]',
      algorithm: 'gzip',
      test: /\.(js|css|html|json|ico|svg|eot|otf|ttf)$/,
      threshold: 81920,
      minRatio: 0.8
    })
  ],
  resolveLoader: {
    modules: [path.resolve(__dirname, 'node_modules')]
  }
}
