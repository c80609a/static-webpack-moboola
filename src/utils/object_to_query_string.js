// to encode a Javascript Object into a string that can be passed via a GET Request
export const object_to_query_string = function(obj, prefix) {
  var str = [],
      p;
  for (p in obj) {
    if (obj.hasOwnProperty(p)) {
      var k = prefix ? prefix + "[" + p + "]" : p,
          v = obj[p];
      str.push((v !== null && typeof v === "object") ?
          object_to_query_string(v, k) :
          encodeURIComponent(k) + "=" + encodeURIComponent(v));
    }
  }
  return str.join("&");
}

// console.log(object_to_query_string({
//   foo: "hi there",
//   bar: {
//     blah: 123,
//     quux: [1, 2, 3]
//   }
// }));
// foo=hi%20there&bar%5Bblah%5D=123&bar%5Bquux%5D%5B0%5D=1&bar%5Bquux%5D%5B1%5D=2&bar%5Bquux%5D%5B2%5D=3