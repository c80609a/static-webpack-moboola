/**
 * Given a string of query parameters creates an object.
 *
 * @example
 * `scope=all&page=2` -> { scope: 'all', page: '2'}
 * `scope=all` -> { scope: 'all' }
 * ``-> {}
 * @param {String} query
 * @returns {Object}
 */
export const parse_query_string_into_object = (query) => {
  const qwery = query || window.location.href.split('?')[1]
  if (!qwery) return {}

  return qwery.split('&').reduce((acc, element) => {
    const val = element.split('=');
    Object.assign(acc, {
      [val[0]]: decodeURIComponent(val[1]),
    });
    return acc;
  }, {});
};