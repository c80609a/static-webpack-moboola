import { set_cookie } from "./set_cookie"

export function delete_cookie(name) {
  set_cookie(name, '', { expires: 0 })
}