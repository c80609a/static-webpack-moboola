export function flattenObject(ob, acc = {}) {
    for(var k in ob) {
      if (typeof ob[k] == 'object') {
        flattenObject(ob[k], acc)
      } else {
        acc[k] = ob[k]
      }
    }
    return acc
}


// var obj = {
//   user: {
//     email:                 "qwert1341y@gmail.com",
//     password:              'password',
//     password_confirmation: 'password',
//     name:                  "QWERTY QWERTY",
//     birthday:              "13.11.1997",
//     gender:                "male",
//     country_id:            "1",
//     browser_lang:          "en"
//   },
//   foo: 'bar'
// }
//
// var res = flattenObject(obj, {})
// console.log(res)
//
// {
//    email: "qwert1341y@gmail.com",
//    password: "password",
//    password_confirmation: "password",
//    name: "QWERTY QWERTY",
//    birthday: "13.11.1997"
//    browser_lang: "en"
//    country_id: "1"
//    email: "qwert1341y@gmail.com"
//    foo: "bar"
//    gender: "male"
//    name: "QWERTY QWERTY"
//    password: "password"
//    password_confirmation: "password"
// }