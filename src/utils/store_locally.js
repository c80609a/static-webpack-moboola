import { set_cookie }    from "./set_cookie"
import { get_cookie }    from "./get_cookie"
import { delete_cookie } from "./delete_cookie"

export const store_locally = function(key, val) {
  localStorage.setItem(key, val)
  set_cookie(key, val, { expires: 60480 })
}

export const get_stored_locally = function(key) {
  return localStorage.getItem(key) || get_cookie(key)
}

export const del_stored_locally = function(key) {
  localStorage.removeItem(key)
  delete_cookie(key)
}

window.get_stored_locally = get_stored_locally