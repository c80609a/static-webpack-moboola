import {POST}               from "./utils/request"
import {get_stored_locally} from "./utils/store_locally"

const URL_BASE = process.env.API_URL

export const api = {
  isEmailNotExist: function (email) {
    return POST(`${URL_BASE}/api/v1/email_exist`, { email: email })
  },

  doRegister: function (password, lang) {
    let params = {
      user: {
        email:                 get_stored_locally('moboola_email_input'),
        password:              password,
        password_confirmation: password,
        name:                  get_stored_locally('moboola_name_input'),
        birthday:              get_stored_locally('moboola_birth_input'),
        gender:                get_stored_locally('moboola_gender_input'),
        country_id:            get_stored_locally('moboola_country_input'),
        browser_lang:          lang
      }
    }

    return POST(`${URL_BASE}/${lang}/users`, params)
  }
}
