import jQuery          from "jquery"
import {store_locally} from "../utils/store_locally"

import 'select2/dist/css/select2.min.css'
import 'select2/dist/js/select2.min.js'
import {createSnackbar} from "@snackbar/core";
import {goLink} from "../utils/go_link";

export class HandleBirthSelects {
  constructor() {
    this.locale   = jQuery('html').attr('lang')
    this.$day     = jQuery('#moboola_day_select')
    this.$month   = jQuery('#moboola_month_select')
    this.$year    = jQuery('#moboola_year_select')
    this.$btn     = jQuery('#moboola_birth_submit')
    this.href     = this.$btn.data('href')
    this.onSubmit = this.onSubmit.bind(this)

    this.init()
  }

  init() {
    if (this.isNoElementsFound()) return

    if (!this.href) throw 'data-href not found'

    this.$day.select2({ minimumResultsForSearch: -1 })
    this.$month.select2({ minimumResultsForSearch: -1 })
    this.$year.select2({ minimumResultsForSearch: -1 })

    this.$btn.on('click', this.onSubmit)
  }

  onSubmit(e) {
    e.preventDefault()
    e.stopImmediatePropagation()

    const value = [
        this.$day.val(),
        this.$month.val(),
        this.$year.val()
    ].join('.')

    if(!!Date.parse(value)) {
      store_locally('moboola_birth_input', value)
      goLink(this.href)
    } else {
      const message = MESSAGES[this.locale] || DEFAULT_MESSAGE
      this.showError(message)
    }
  }

  isNoElementsFound() {
    return !(this.$day.length == 1 && this.$month.length == 1 && this.$year.length == 1 && this.$btn.length == 1)
  }

  showError(message) {
    createSnackbar(message, {
      timeout:  10000,
      position: 'left',
      actions:  []
    })
  }
}

const MESSAGES        = { ru: 'Укажите, пожалуйста, дату рождения', en: 'Please, enter your date of birth' }
const DEFAULT_MESSAGE = MESSAGES['en']