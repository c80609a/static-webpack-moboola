import jQuery             from 'jquery'
import { createSnackbar } from '@snackbar/core'
import { store_locally }  from "../utils/store_locally"
import {goLink}           from "../utils/go_link"

export class HandleNameInput {
  constructor() {
    this.locale   = jQuery('html').attr('lang')
    this.$input   = jQuery('#moboola_name_input')
    this.$button  = jQuery('#moboola_name_submit')
    this.href     = this.$button.data('href')
    this.onSubmit = this.onSubmit.bind(this)

    this.init()
  }

  init() {
    if (this.isNoElementsFound()) return
    // alert('init')

    if (!this.href) throw 'data-href not found'

    this.$input.prop('maxlength', MAX_LEN)
    this.$button.on('click', this.onSubmit)
  }

  onSubmit(e) {
    e.preventDefault()
    e.stopImmediatePropagation()

    if (this.isNameValid()) {
      this.doContinue()
    } else {
      this.showError()
    }
  }

  isNameValid() {
    const name = this.$input.val()
    return name.length >= MIN_LEN && name.length <= MAX_LEN
  }

  doContinue() {
    const name = this.$input.val()
    store_locally('moboola_name_input', name)
    goLink(this.href)
  }

  showError() {
    const message = MESSAGES[this.locale] || DEFAULT_MESSAGE
    createSnackbar(message, {
      timeout:  10000,
      position: 'left',
      actions:  []
    })
  }

  isNoElementsFound() {
    return !(this.$input.length == 1 && this.$button.length == 1)
  }
}

const MIN_LEN = 2
const MAX_LEN = 64

const MESSAGES        = { ru: 'Укажите Ваше имя, пожалуйста', en: 'Please, enter your name' }
const DEFAULT_MESSAGE = 'Please, enter your name'