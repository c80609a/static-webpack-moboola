import { store_locally }   from "../utils/store_locally"
import {get_param_by_name} from "../utils/get_param_by_name"

export class HandleParams {
  constructor() {
    const affilate_id = get_param_by_name('affilate_id')
    const click_id    = get_param_by_name('click_id')
    const cpa         = get_param_by_name('cpa')
    const t           = get_param_by_name('t')

    if (!!affilate_id && !!click_id && !!cpa && !!t) {
      store_locally('affilate_id', affilate_id)
      store_locally('click_id', click_id)
      store_locally('cpa', cpa)
      store_locally('t', t)
    }
  }
}