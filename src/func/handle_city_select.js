import jQuery           from "jquery"
import {store_locally}  from "../utils/store_locally"
import {createSnackbar} from "@snackbar/core"

import '../lib/select2/en'
import '../lib/select2/ru'
import 'select2/dist/css/select2.min.css'
import 'select2/dist/js/select2.min.js'
import {goLink} from "../utils/go_link";

export class HandleCitySelect {
  constructor() {
    this.locale   = jQuery('html').attr('lang')
    this.$country = jQuery('#moboola_country_select')
    this.$btn     = jQuery('#moboola_country_submit')
    this.href     = this.$btn.data('href')

    this.onSubmit = this.onSubmit.bind(this)

    this.init()
  }

  init() {
    if (this.isNoElementsFound()) return

    if (!this.href) throw 'data-href not found'

    const f_data = function(params) {
      return { q: params.term }
    }

    const f_rslt = function(data) {
      return {
        results: jQuery.map(data, function(country) {
          return { id: country.id, text: country.titleEn }
        })
      }
    }

    this.$country.select2({
      language:           this.locale,
      minimumInputLength: 1,
      ajax: {
        dataType:         'json',
        delay:            250,
        url:              `${process.env.API_URL}/${this.locale}/countries.json`,
        cache:            true,
        data:             f_data,
        processResults:   f_rslt
      }
    })

    this.$btn.on('click', this.onSubmit)
  }

  onSubmit(e) {
    e.preventDefault()
    e.stopImmediatePropagation()

    const value = this.$country.val()

    if (!value) {
      const message = MESSAGES[this.locale] || DEFAULT_MESSAGE
      this.showError(message)

      return
    }

    store_locally('moboola_country_input', value)
    goLink(this.href)
  }

  isNoElementsFound() {
    return !(this.$country.length == 1 && this.$btn.length == 1)
  }

  showError(message) {
    createSnackbar(message, {
      timeout:  10000,
      position: 'left',
      actions:  []
    })
  }
}

const MESSAGES        = { ru: 'Укажите страну, пожалуйста', en: 'Please, enter your country' }
const DEFAULT_MESSAGE = MESSAGES['en']