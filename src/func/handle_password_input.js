import jQuery                   from 'jquery'
import { createSnackbar }       from '@snackbar/core'
import {api}                    from "../api"
import {get_stored_locally}     from "../utils/store_locally"
import {object_to_query_string} from "../utils/object_to_query_string"

export class HandlePasswordInput {
  constructor() {
    this.locale   = jQuery('html').attr('lang')
    this.$input   = jQuery('#moboola_password_input')
    this.btnId    = 'moboola_password_submit'
    this.$button  = jQuery('#'+this.btnId)
    this.onClick  = this.onClick.bind(this)

    this.init()
  }

  init() {
    if (this.isNoElementsFound()) return
    // alert('init')

    this.$input.prop('maxlength', MAX_LEN)
    this.$button.on('click', this.onClick)
  }

  onClick(e) {
    if (this.submitting) return

    e.preventDefault()
    e.stopImmediatePropagation()

    if (this.isPasswordValid()) {
      this.submitting = true
      const that = this
      setTimeout(() => { that.sendRequest() }, 500)
    } else {
      const message = MESSAGES[this.locale] || DEFAULT_MESSAGE
      this.showError(message)
    }
  }

  isPasswordValid() {
    const password = this.$input.val()

    const any_error = [
      function(value) { return value.length >= MIN_LEN },
      function(value) { return value.length <= MAX_LEN },
    ].map(fun => fun(password)).some(result => !result)

    return !any_error
  }

  sendRequest() {
    const password = this.$input.val()
    const that = this
    api.doRegister(password, this.locale).then((data) => {
      console.log(data)
      // {"id":54011,"name":"123","gender":"male","fast_auth_token":"3be2c6a4213e6ca5f8567e06c732698d"}
      const at = data.fast_auth_token || data.fastAuthToken
      console.log(at)
      if (at) {
        let query_params = { auth: at }

        const affilate_id = get_stored_locally('affilate_id')
        const click_id    = get_stored_locally('click_id')
        const cpa         = get_stored_locally('cpa')
        const t           = get_stored_locally('t')
        if (!!affilate_id && !!click_id && !!cpa && !!t) {
          query_params.affilate_id = affilate_id
          query_params.click_id    = click_id
          query_params.cpa         = cpa
          query_params.t           = t
        }
        const url = `${process.env.API_URL}/${this.locale}/users/sign_in?${object_to_query_string(query_params)}`
        console.log(url)
        window.location.href = url
      }
    }).catch(
        (err) => {
          setTimeout(function() { that.submitting = false }, 500)
        }
    )
  }

  showError(message) {
    createSnackbar(message, {
      timeout:  10000,
      position: 'left',
      actions:  []
    })
  }

  isNoElementsFound() {
    return !(this.$input.length == 1 && this.$button.length == 1)
  }

  set submitting(val) {
    if (val) {
      Loading.showThrobber(this.btnId)
      this.$button.css('color', 'transparent')
      this.$button.attr('disabled', true)
      this.$button.css('pointer-events', 'none')
      this.$button.css('position', 'relative')
    } else {
      this.$button.css('color', '#fff')
      this.$button.removeAttr('disabled')
      this.$button.css('pointer-events', 'all')
      Loading.hide()
    }
    this._submitting = val
  }

  get submitting() {
    return this._submitting
  }
}

const MIN_LEN = 2
const MAX_LEN = 20

const MESSAGES        = { ru: 'Придумайте ваш пароль, пожалуйста', en: 'Please, specify your password' }
const DEFAULT_MESSAGE = MESSAGES['en']