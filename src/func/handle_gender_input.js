import jQuery          from "jquery"
import {store_locally} from "../utils/store_locally"

export class HandleGenderInput {
  constructor() {
    this.$button_male   = jQuery('#moboola_submit_gender_man')
    this.$button_female = jQuery('#moboola_submit_gender_woman')

    this.onSubmitMale   = this.onSubmitMale.bind(this)
    this.onSubmitFemale = this.onSubmitFemale.bind(this)

    this.init()
  }

  init() {
    if (this.isNoElementsFound()) return

    this.$button_male.on('click', this.onSubmitMale)
    this.$button_female.on('click', this.onSubmitFemale)
  }

  onSubmitMale() {
    store_locally('moboola_gender_input', 'male')
  }

  onSubmitFemale() {
    store_locally('moboola_gender_input', 'female')
  }

  isNoElementsFound() {
    return !(this.$button_male.length == 1 && this.$button_female.length == 1)
  }
}