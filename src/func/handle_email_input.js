import jQuery             from 'jquery'
import { createSnackbar } from '@snackbar/core'
import { store_locally }  from "../utils/store_locally"
import {goLink}           from "../utils/go_link"
import {api}              from "../api"

export class HandleEmailInput {
  constructor() {
    this.locale   = jQuery('html').attr('lang')
    this.$input   = jQuery('#moboola_email_input')
    this.btnId    = 'moboola_email_submit'
    this.$button  = jQuery('#' + this.btnId)
    this.href     = this.$button.data('href')
    this.onClick  = this.onClick.bind(this)

    this.init()
  }

  init() {
    if (this.isNoElementsFound()) return
    // alert('init')

    if (!this.href) throw 'data-href not found'

    this.$input.prop('maxlength', MAX_LEN)
    this.$button.on('click', this.onClick)
  }

  onClick(e) {
    e.preventDefault()
    e.stopImmediatePropagation()

    if (this.isEmailValid()) {
      this.submitting = true
      const that = this
      setTimeout(() => { that.sendRequest() }, 500)
    } else {
      const message = MESSAGES[this.locale] || DEFAULT_MESSAGE
      this.showError(message)
    }
  }

  isEmailValid() {
    const name = this.$input.val()

    const any_error = [
      function(value) { return value.length >= MIN_LEN },
      function(value) { return value.length <= MAX_LEN },
      function(value) { return EMAIL.test(value) },
    ].map(fun => fun(name)).some(result => !result)

    return !any_error
  }

  sendRequest() {
    const name = this.$input.val()
    const that = this
    api.isEmailNotExist(name).then((data) => {
      const is_exist = data.detail
      // console.log('exists? = ' + is_exist)

      if (is_exist) {
        const message = MESSAGES_EMAIL[this.locale] || DEFAULT_MESSAGES_EMAIL
        this.showError(message)
      } else {
        this.doContinue()
      }
    }).catch(
        (err) => {
          setTimeout(function() { that.submitting = false }, 1000)
        }
    )
  }

  doContinue() {
    const name = this.$input.val()
    store_locally('moboola_email_input', name)
    goLink(this.href)
  }

  showError(message) {
    createSnackbar(message, {
      timeout:  10000,
      position: 'left',
      actions:  []
    })
  }

  isNoElementsFound() {
    return !(this.$input.length == 1 && this.$button.length == 1)
  }

  set submitting(val) {
    if (val) {
      Loading.showThrobber(this.btnId)
      this.$button.css('color', 'transparent')
      this.$button.attr('disabled', true)
      this.$button.css('pointer-events', 'none')
      this.$button.css('position', 'relative')
    } else {
      this.$button.css('color', '#fff')
      this.$button.removeAttr('disabled')
      this.$button.css('pointer-events', 'all')
      Loading.hide()
    }
    this._submitting = val
  }

  get submitting() {
    return this._submitting
  }
}

const MIN_LEN = 2
const MAX_LEN = 64
const EMAIL   = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

const MESSAGES        = { ru: 'Укажите ваш email, пожалуйста', en: 'Please, enter your email' }
const DEFAULT_MESSAGE = MESSAGES['en']

const MESSAGES_EMAIL         = { ru: 'Такой email уже зарегистрирован', en: 'Email already exists' }
const DEFAULT_MESSAGES_EMAIL = MESSAGES_EMAIL['en']