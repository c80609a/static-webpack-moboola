import '@snackbar/core/dist/snackbar.css'
import './app.scss'

import { HandleParams }        from "./func/handle_params"
import { HandleGenderInput }   from "./func/handle_gender_input"
import { HandleNameInput }     from "./func/handle_name_input"
import { HandleEmailInput }    from "./func/handle_email_input"
import { HandlePasswordInput } from "./func/handle_password_input"
import { HandleBirthSelects }  from "./func/handle_birth_selects"
import { HandleCitySelect }    from "./func/handle_city_select"

class App {
  constructor() {
    new HandleParams()
    new HandleGenderInput()
    new HandleNameInput()
    new HandleEmailInput()
    new HandlePasswordInput()
    new HandleBirthSelects()
    new HandleCitySelect()
  }
}

function start() {
  new App()
}

if (/complete|interactive|loaded/.test(document.readyState)) {
  start()
} else {
  window.addEventListener('DOMContentLoaded', start)
}
